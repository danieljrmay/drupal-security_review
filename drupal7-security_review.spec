%{?drupal7_find_provides_and_requires}

%global module security_review

Name:          drupal7-%{module}
Version:       1.3
Release:       1%{?dist}
Summary:       Automates testing for many of the easy-to-make mistakes that render your drupal 7 site insecure

License:       GPLv2+
URL:           http://drupal.org/project/%{module}
Source0:       http://ftp.drupal.org/files/projects/%{module}-7.x-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: drupal7-rpmbuild >= 7.23-3

Requires:      drupal7


%description
The Security Review module automates testing for many of the
easy-to-make mistakes that render your site insecure.

This package provides the following Drupal module:
* %{module}


%prep
%setup -qn %{module}


%build
# Empty build section, nothing to build


%install
mkdir -p %{buildroot}%{drupal7_modules}/%{module}
cp -pr * %{buildroot}%{drupal7_modules}/%{module}/


%files
%license LICENSE.txt
%doc README.txt API.txt
%{drupal7_modules}/%{module}
%exclude %{drupal7_modules}/%{module}/API.txt
%exclude %{drupal7_modules}/%{module}/LICENSE.txt
%exclude %{drupal7_modules}/%{module}/README.txt


%changelog
* Tue Nov 27 2018 Daniel J. R. May <daniel.may@kada-media.com> - 1.3-1
- Initial release of RPM packaging.
