# drupal-security_review

RPM packaging for the Drupal Security Review module.

See https://www.drupal.org/project/security_review


## Copr Respository 

If you are using Fedora, RHEL or CentOS (or similar) then you can
install the `drupal7-security_review` RPM via a [Copr
repository](https://copr.fedorainfracloud.org/coprs/danieljrmay/drupal-packages/).

```console
$ dnf copr enable danieljrmay/drupal-packages
$ dnf install drupal7-security_review
```
